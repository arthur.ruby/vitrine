<?php

namespace App\Controller;

use App\Entity\Unicorn;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UnicornController extends AbstractController
{
    /**
     * @Route("/unicorn", name="app_unicorn")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $allUnicorns = $doctrine->getRepository(Unicorn::class)->findAll();
		return $this->render('unicorn/index.html.twig', [
            'controller_name' => 'UnicornController',
			'unicorns' => $allUnicorns
        ]);
    }
}
